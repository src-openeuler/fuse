%global fuse2ver 2.9.9

Name:    fuse
Version: %{fuse2ver}
Release: 11
Summary: User space File System of fuse2
License: GPL+ and LGPLv2+
URL:     http://fuse.sf.net
Source0: https://github.com/libfuse/libfuse/archive/%{name}-%{fuse2ver}.tar.gz

Patch0:        0000-fix-compile-error-because-of-ns-colliding.patch
Patch1:        0001-libfuse-Assign-NULL-to-old-to-avoid-free-it-twice-52.patch
Patch2:        0002-util-ulockmgr_server.c-conditionally-define-closefro.patch
Patch3:        0003-add-fuse-test-dir.patch

BuildRequires: libselinux-devel, pkgconfig, systemd-udev, meson, fdupes
BuildRequires: autoconf, automake, libtool, gettext-devel, ninja-build
Requires:      which, fuse-common
Recommends:    %{name}-help = %{version}-%{release}
Conflicts:     filesystem < 3
Provides:      %{name}-libs
Obsoletes:     %{name}-libs

%description
FUSE (Filesystem in Userspace) is an interface for userspace programs to export
a filesystem to the Linux kernel. The FUSE project consists of two components:
the fuse kernel module (maintained in the regular kernel repositories) and the
libfuse userspace library (maintained in this repository). libfuse provides the
reference implementation for communicating with the FUSE kernel module.


%package devel
Version:        %{fuse2ver}
Summary:        User space File System of fuse2 devel files
Obsoletes:      %{name}-libs
Requires:       pkgconfig
License:        LGPLv2+
Conflicts:      filesystem < 3

%description devel
This package contains all include files, libraries and configuration
files needed to develop programs that use the fuse2.

%package help
Summary: Including man files for fuse
Requires: man

%description    help
This contains man files for the using of fuse

%prep
%autosetup -n %{name}-%{fuse2ver} -p1

#if change configure.ac, should autoreconf
autoreconf -ivf 

%build
export MOUNT_FUSE_PATH="%{_sbindir}"
export CFLAGS="%{optflags} -D_GNU_SOURCE"
%configure --enable-lib
%make_build

%install
%make_install

rm -f %{buildroot}/%{_libdir}/*.a
rm -f %{buildroot}%{_sysconfdir}/init.d/fuse
rm -f %{buildroot}%{_sysconfdir}/udev/rules.d/99-fuse.rules
find %{buildroot} -type f -name "*.la" -delete -print


%post -n fuse -p /sbin/ldconfig
%postun -n fuse -p /sbin/ldconfig

%files
%doc {AUTHORS,ChangeLog,NEWS,README*}
%license COPYING
%{_sbindir}/mount.fuse
%attr(4755,root,root) %{_bindir}/fusermount
%{_bindir}/ulockmgr_server
%{_libdir}/libfuse.so.*
%{_libdir}/libulockmgr.so.*

%files devel
%{_includedir}/fuse.h
%{_includedir}/ulockmgr.h
%{_includedir}/fuse
%{_libdir}/libfuse.so
%{_libdir}/libulockmgr.so
%{_libdir}/pkgconfig/fuse.pc

%files help
%{_mandir}/man1/*
%{_mandir}/man8/*


%changelog
* Thu Apr 28 2022 zhanchengbin <zhanchengbin1@huawei.com> -2.9.9-11
- fix can not print changelog messages

* Mon Apr 11 2022 zhanchengbin <zhanchengbin1@huawei.com> -2.9.9-10
- add fuse test dir

* Wed Aug 11 2021 lixiaokeng <lixiaokeng@huawei.com> -2.9.9-9
- fix double define closefrom

* Mon Jul 26 2021 lixiaokeng <lixiaokeng@huawei.com> -2.9.9-8
- autosetup patch

* Sat Jun 19 2021 yanglongkang <yanglongkang@huawei.com> -2.9.9-7
- fix changelog error

* Mon May 24 2021 yanglongkang <yanglongkang@huawei.com> -3.9.2-6
- Separate fuse3 out into its own package

* Sun May 23 2021 yanglongkang <yanglongkang@huawei.com> -3.9.2-5
- temporarily fallback code for build

* Sat May 22 2021 yanglongkang <yanglongkang@huawei.com> -3.9.2-4
- Separate fuse3 out into its own package

* Thu Dec 17 2020 yanglongkang <yanglongkang@huawei.com> -3.9.2-3
- set help package as install requires

* Mon Jul 13 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> -3.9.2-2
- backport upstream bugfix patches

* Mon Jul 06 2020 Youming Zhang <zhangyouming4@huawei.com> -3.9.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update fuse3 from 3.9.0 to 3.9.2

* Tue Jun 30 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 2.9.9-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:renumber patches

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.9.9-0
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update fuse2 from 2.9.7 to 2.9.9,
       update fuse3 from 3.2.3 to 3.9.0

* Sat Dec 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.9.7-23
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:sync bugfix from community

* Wed Sep 4 2019 zoujing <zoujing13@huawei.com> - 2.9.7-22
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESCi:openEuler Debranding

* Fri Aug 23 2019 zoujing <zoujing13@huawei.com> - 2.9.7-21
- Type:enhancement
- ID:NA
- SUG:restart
- DESC: remove sensitive information

* Thu Aug 22 2019 zoujing <zoujing13@huawei.com> - 2.9.7-20
- Type: enhancement
- ID:NA
- SUG:restart
- DESC:rename patch name

* Mon Aug 12 2019 huangzheng <huangzheng22@huawei.com> - 2.9.7-19
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:safety code review, delete sensitive information

* Tue Jul 23 2019 Shijie Luo <luoshijie1@huawei.com> - 2.9.7-18
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix fuse crash problem when rm node

* Fri Apr 19 2019 wangchan <wangchan9@huawei.com> - 2.9.7-17
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: bfuse-fix-fs-cleanup
        fuse_opt_parse(): fix memory leak
        fusermount: refuse unknown options
        fusermount: whitelist known-good filesystems for mountpoints
        fusermount: Fix memory leaks
        Fix invalid free of memory pointer in 'struct fuse_buf'
        Fix memory leak of FUSE modules

* Fri Mar 22 2019 yangjian<yangjian79@huawei.com> - 2.9.7-16
- Type:cves
- ID:CVE-2018-10906
- SUG:NA
- DESC:fix CVE-2018-10906

* Fri Jan 25 2019 liuqianya<liuqianya@huawei.com> - 2.9.7-15
- Type:bugfix
- ID:NA
- SUG:NA
  DESC:increase idle thread
       invalid free and core
       memory leak and wild ptr
       fuse exit when got EINVAL error

* Wed Jul 18 2018 openEuler Buildteam <buildteam@openeuler.org> - 2.9.7-14
- Package init

